<?php
        
include_once "../modele/CompteManage.php";

if (!isset($_SESSION))
        {
          session_start();
        }

$Adresse = $_SESSION["Identifiant"];
$Mdp = $_SESSION["MotDePasse"];
$You = new Compte($Adresse, $Mdp);
$You->logout();

include "../vue/enteteNonCompte.php";
include "../vue/vueDeconnexion.php";
include "../vue/pied.php";

?>