<!DOCTYPE html>

<html>
    <head>
    	<meta charset="utf-8">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/css/materialize.min.css">
        <style>
        body {
            display: flex;
            min-height: 100vh;
            flex-direction: column;
        }

        main {
            flex: 1 0 auto;
        }

        body {
            background: #e4e4e4;
        }
        
        nav.nav-center ul {
        text-align: center;
        }
        nav.nav-center ul li {
            display: inline;
            float: none;
        }
        nav.nav-center ul li a {
            display: inline-block;
        }
        </style>
    </head>
    
    <body>
        <nav>
          <div class="grey darken-3 nav-wrapper">
          	
          </div>
        </nav>
        