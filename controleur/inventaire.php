<?php
//mettre en include_once ce qui correspond dans le modele



include_once "../modele/CompteManage.php";
include_once "../modele/SalleManage.php";
include_once "../modele/InventaireManage.php";


if (!isset($_SESSION))
        {
          session_start();
        }

if (!isset($_SESSION["Identifiant"]) || !isset($_SESSION["MotDePasse"]))
{

    $Inv = new Inv();
    $AllInv = $Inv->getInvByType("CM");
    $lesInv = "";
    
    foreach ($AllInv as $InfoInv)
    {
        $lesInv = $lesInv."<tr><td><center>".$InfoInv["Quantité"]."</td><td><center>".$InfoInv["Nom"]."</td><td><center>".$InfoInv["Localisation"]."</td></tr> ";
    }

    $lesTypes = "";
    $AddBtn = "";
    include "../vue/enteteNonCompte.php";
    echo str_replace(array("%%Info%%","%%Type%%","%%Add&Rem%%"),array($lesInv,$lesTypes,$AddBtn),file_get_contents("../vue/vueInventaire.php"));
    include "../vue/pied.php";
}
else
{
$Identifiant = $_SESSION["Identifiant"];
$Mdp = $_SESSION["MotDePasse"];
$test = new Compte($Identifiant, $Mdp);
if ($test->isLoggedOn())
{

    if (isset($_GET["type"]))
    {
        $type = $_GET["type"];
    }
    else
    {
        $type = "";
    }

    $Inv = new Inv();

    $AllInv = $Inv->getInvByType($type);
    $lesInv = "";

    $AllTypes = $Inv->getType();
    $lesTypes = "";
    
    foreach ($AllInv as $InfoInv)
    {
        $lesInv = $lesInv."<tr><td><center>".$InfoInv["Quantité"]."</td><td><center>".$InfoInv["Nom"]."</td><td><center>".$InfoInv["Localisation"]."</td><td><a href=\"../controleur/ModifInv.php?action=del&nom=".$InfoInv["Nom"]."&loc=".$InfoInv["Localisation"]."\"><img class=\"responsive-img\" style=\"width: 20px;\" src=\"../image/cross.png\" /></a></td></tr> ";
    }

    $countTypes=count($AllTypes);

    if($countTypes==0)
    {
        $countTypes=1;
    }
    $width = (100/$countTypes);

    foreach ($AllTypes as $InfoTypes)
    {
        $lesTypes = $lesTypes."<ul id=\"nav-mobile\" class=\"left hide-on-med-and-down grey darken-2\" style=\"width: ".$width."%\"><li><a style=\"width: auto\" class=\"btn-large grey darken-3\" href=\"?type=".$InfoTypes["Type"]."\">".$InfoTypes["Type"]."</a></li></ul>";
    }

    $Info = $test->getCompteById($_SESSION["Identifiant"]);

    $AddBtn = 
        "
        <form name=\"page_modif\" action=\"../controleur/ModifInv.php\" method=\"post\">
        <div class=\"container\">
        <div class=\"z-depth-1 grey lighten-4 row\" style=\"display: inline-block; padding: 32px 48px 0px 48px; border: 1px solid #EEE; border-radius: 20px\">
        <table class=\"centered\">
        <tr><td>Quantité</td><td><input class=\"validate\" type=\"number\" name=\"Qtte\" style=\"width: 60%\"></td></tr>
        <tr><td>Nom</td><td><input class=\"validate\" type=\"text\" name=\"Nom\" style=\"width: 60%\"></td></tr>
        <tr><td>Localisation</td><td><input class=\"validate\" type=\"text\" name=\"Loca\" style=\"width: 60%\"></td></tr>
        <tr><td>Type</td><td><input class=\"validate\" type=\"text\" name=\"Type\" style=\"width: 60%\"></td></tr>
        </table>
        <input type=\"submit\" class=\"btn green darken-2\" name=\"action\" value=\"Ajouter\">
        
        </div>
        </div>
        </form>
        ";

    echo str_replace("%%Balisto%%",$Info["Libelle"],file_get_contents("../vue/enteteCompte.php"));
    echo str_replace(array("%%Info%%","%%Type%%","%%Add&Rem%%"),array($lesInv,$lesTypes,$AddBtn),file_get_contents("../vue/vueInventaire.php"));
    include "../vue/pied.php";
}
else
{

    $Inv = new Inv();
    $AllInv = $Inv->getInvByType("CM");
    $lesInv = "";
    
    foreach ($AllInv as $InfoInv)
    {
        $lesInv = $lesInv."<tr><td><center>".$InfoInv["Quantité"]."</td><td><center>".$InfoInv["Nom"]."</td><td><center>".$InfoInv["Localisation"]."</td></tr> ";
    }

    $lesTypes = "";
    $AddBtn = "";
    include "../vue/enteteNonCompte.php";
    echo str_replace(array("%%Info%%","%%Type%%","%%Add&Rem%%"),array($lesInv,$lesTypes,$AddBtn),file_get_contents("../vue/vueInventaire.php"));
    include "../vue/pied.php";
}
}


?>
