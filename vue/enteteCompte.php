<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8" http-equiv="refresh" content="90">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/css/materialize.min.css">
        <style>
        body {
            display: flex;
            min-height: 100vh;
            flex-direction: column;
        }

        main {
            flex: 1 0 auto;
        }

        body {
            background: #e4e4e4;
        }
        
        nav.nav-center ul {
        text-align: center;
        }
        nav.nav-center ul li {
            display: inline;
            float: none;
        }
        nav.nav-center ul li a {
            display: inline-block;
        }
        </style>
    </head>
    
    <body>
        <nav>
          <div class="grey darken-3 nav-wrapper">
          	<ul id="nav-mobile" class="left hide-on-med-and-down grey darken-2 disabled">
              <li><a href="../controleur/accueil.php">Info Salle</a></li>
            </ul>
            <ul id="nav-mobile" style="width: 10px" class="left grey darken-3">
              <li><a href="#"></a><br></li>
            </ul>
            <ul id="nav-mobile" class="left hide-on-med-and-down grey darken-2">
              <li><a href="../controleur/inventaire.php">Inventaire</a></li>
            </ul>
            <a href="../controleur/accueil.php" class="brand-logo center hide"><img class="responsive-img" style="height: 90px; width: auto;" src="../image/Logo.jpg" /></a>
            <ul id="nav-mobile" class="right hide-on-med-and-down">

              <li><a>Compte : %%Balisto%%</a></li>
              <li><a class="grey darken-2 orange-text" href="../controleur/deconnexion.php">Déconnexion</a></li>
            </ul>
          </div>
        </nav>
        