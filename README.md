# Emploi du temps TICE et Salles info 

Mise en place d'un système de gestion de l'emploi du temps des salles informatique et celui du TICE du collège Georges Charpak

## Cahier des Charges

![CdC](Cahier des Charges/CDC.png)

## Cas Utilisation

![Cas](Cahier des Charges/Cas_Utilisation.png)

## Architecture Logiciel

```mermaid
graph TD;
  Accueil-->Vu_Horaires_Salles;
  Accueil-->Règlages_Langues;
  Accueil-->Connexion;

  Connexion--Modification-->Vu_Horaires_TICE;
  Connexion-->Vu_Horaires_Salles;
  
  Accueil-->Vu_Horaires_TICE;

  Vu_Horaires_Salles-->Réservation_Salle;
  Code_Suppression-->Vu_Horaires_Salles;
  Réservation_Salle-->Code_Suppression;
  Vu_Horaires_Salles-->Remove_Utilisation_Salle;

```
## COCOMO 2

Le modèle COCOMO 2 est une méthode permettant de prévoire le temps, le coût et le nombre de personnes nécessaire à la réalisation d'un projet informatique. Pour cette prédiction, le modèle se base sur le nombre de lignes/fonctions, de la qualification des programmeur, des logiciels et des plateformes utilisées.

![cocomo](Cahier des Charges/CocomoII.png)

## Diagramme de classes

![classes](Cahier des Charges/Diagramme.png)

### MCD de la Base DE Données

![BDD](Cahier des Charges/BDD.png)
