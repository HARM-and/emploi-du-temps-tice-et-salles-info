<div class="section"></div>
    <main>
        <center>
            <div class="container">
                <div class="z-depth-1 grey lighten-4 row" style="display: inline-block; padding: 32px 48px 0px 48px; border: 1px solid #EEE; border-radius: 20px; width: 80%">

                    %%Add&Rem%%
                    
                        <table class="striped grey lighten-2 bordered" style="overflow: scroll;">

                            <tablehead>
                                <tr>
                                    <td class="grey lighten-1" style="text-align: center; width: 30%">N° de la salle</td>
                                    <td class="grey lighten-1" style="width: 1%"></td>
                                    <td class="grey lighten-1" style="text-align: center">Information relative à la salle</td>
                                    <td class="grey lighten-1" style="width: 30px; text-align: center"></td>
                                </tr>
                            </tablehead>
                            <tablebody>
                                %%Info%%
                            </tablebody>
                        </table>
                    </a>
                    <br>
                </div>
                
            </div>

        </center>
                
    </main>
<div class="section"></div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>

