<?php

include_once "../modele/BDManage.php";

class Salle
{
  private $BDD;

  /**
   * Constructeur de la classe
   */
  function __construct()
  {
    $this->BDD = new Data;
      // Ouverture d'une session si elle ne l'est pas encore
    if (!isset($_SESSION))
    {
      session_start();
    }
  }

  /**
   * Obtenir la liste des salle trié par Info et par N°.
   *
   * @return     array  Liste des salles.
   */
  function getSalle()
  {
    $resultat = array();
      // Préparation et envoie d'une requête SQL
    try
    {
        // Connexion à la bdd
      $cnx = $this->BDD->connexionPDO();
        // Préparation de la requête
      $req = $cnx->prepare("select * from salle order by Info desc, N°");
        // Execution de la requête
      $req->execute();
      // Réupération de la réponse SQL
      $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
    }
      // En cas d'erreur
    catch (PDOException $e)
    {
      print "Erreur !: " . $e->getMessage();
      die();
    }
    return $resultat;
  }

  /**
   * Gets the salle avec information.
   *
   * @return     array  The salle avec information.
   */
  function getSalleAvecInfo()
  {
    $resultat = array();
      // Préparation et envoie d'une requête SQL
    try
    {
        // Connexion à la bdd
      $cnx = $this->BDD->connexionPDO();
        // Préparation de la requête
      $req = $cnx->prepare("select * from salle where Info is not null order by N°");
        // Execution de la requête
      $req->execute();
        // Réupération de la réponse SQL
      $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
    }
      // En cas d'erreur
    catch (PDOException $e)
    {
      print "Erreur !: " . $e->getMessage();
      die();
    }
    return $resultat;
  }
  /**
   * Ajouter une salle.
   *
   * @param      int  $value  Le numéro de la salle
   */
  function AddSalle( $value)
  {
      // Préparation et envoie d'une requête SQL
    try
    {
        // Connexion à la bdd
      $cnx = $this->BDD->connexionPDO();
        // Préparation de la requête
      $req = $cnx->prepare("insert into `salle` (`N°`, `Info`) VALUES ('".$value."', NULL)");
        // Execution de la requête
      $req->execute();
    }
      // En cas d'erreur
    catch (PDOException $e)
    {
      //Something went wrong so I need to redirect
      header("Location: ../index.php");
      // Always make an explicit call to exit() after a redirection header.
      exit();
    }
  }

  /**
   * Supprime une salle
   *
   * @param      int   $value  Le numéro de la salle
   */
  function DelSalle( $value)
  {
      // Préparation et envoie d'une requête SQL
    try
    {
        // Connexion à la bdd
      $cnx = $this->BDD->connexionPDO();
        // Préparation de la requête
      $req = $cnx->prepare("delete from `salle` WHERE N°=".$value."");
        // Execution de la requête
      $req->execute();
    }
      // En cas d'erreur
    catch (PDOException $e)
    {
      //Something went wrong so I need to redirect
      header("Location: ../index.php");
      // Always make an explicit call to exit() after a redirection header.
      exit();
    }
  }

  /**
   * Reformate un numéro
   *
   * @param      int|string  $value  Le numéro de départ
   *
   * @return     string  ( Le numéro de départ ramené à une suite de minimum 3 chiffres en completant avec des 0 )
   */
  function completeNum($value)
  {
    $resultat = $value;
      // Tant que la chaîne est trop courte
    while (strlen($resultat)<3)
    {
      $resultat = "0".$resultat;
    }
    return $resultat;
  }

  /**
   * Vérifier si la variable entrante est vide
   *
   * @param      string  $content  Contenu du champ 'INFO' de la bdd
   *
   * @return     string  (Colorisation d'une cellule)
   */
  function infoIsSet( $content)
  {
    $resultat = "";

    if(isset($content))
    {
      $resultat = "class=\"yellow lighten-3\"";
    }

    return $resultat;
  }

  /**
   * Insérer un info pour une salle.
   *
   * @param      string      $text   Info à ajouter
   * @param      int|string  $num    Numéro de la salle à modifier
   */
  function setInfo( $text, $num)
  {
      // Connexion à la bdd
    $cnx = $this->BDD->connexionPDO();
      // Préparation de la requête
    $req = $cnx->prepare("update salle set info=? where salle.N°=?");
      // Execution de la requête
    $req->execute([$text,$num]);
  }
}


?>